require(
     [
         'jquery',
         'splunkjs/mvc',
         'splunkjs/mvc/tokenutils',
         'splunkjs/mvc/searchmanager',
         'splunkjs/mvc/savedsearchmanager',
         'splunkjs/mvc/simplexml/urltokenmodel',
         'splunkjs/mvc/postprocessmanager'

     ],
     function ($, mvc,TokenUtils,SearchManager,SavedSearchManager,UrlTokenModel,PostProcessManager) {

           var service = mvc.createService({ owner: "nobody" });
         // Delete Button Ref
         const delete_config_button = $(`<button id="deleteRecordButton" class="btn btn-primary my-btn">Delete Record</button>`);

         // Place it
            $('.dashboard-form-globalfieldset').append(delete_config_button);
         // Delete Button
            $("#deleteRecordButton").click(function() {
            // Get the value of the key ID field
            var tokens = mvc.Components.get("default");
            var form_keyid = tokens.get("KeyID");

            // Delete the record that corresponds to the key ID using
            // the del method to send a DELETE request
            // to the storage/collections/data/{collection}/ endpoint
            service.del("storage/collections/data/dashboard_input_settings/" + encodeURIComponent(form_keyid)) 
                        
        });

             });

